# @tangle/overwrite

see tests for examples.

## API

### `Overwrite(opts) => overwrite`

opts *Object* (optional) can have properties:
- `identity`
    - define the identity transformation for this strategy
    - default `{}`
- `reifiedIdentity`
    - define with `reify(identity)` resolves to
    - default `null`,
- `valueSchema` *Object*
    - add a JSON-schema to validate the values which are allowed to be passed in
    - default (see code, allows string / object / boolean / number)

### `overwrite.schema`
### `overwrite.isValid`
### `overwrite.identity() => I`
### `overwrite.concat(R, S) => T`
### `overwrite.mapFromInput(input, currentTips) => T`
### `overwrite.mapToOutput(T) => t`

### `overwrite.isConflict(graph, nodeIds, field) => Boolean`

where:
- `graph` is a `@tangle/graph` instance
- `nodeIds` is an Array of nodeIds you're wanting to check for conflict
- `field` *String* is the data field `node.data[field]` you're checking for conflicts

### `overwrite.isValidMerge(graph, mergeNode, field) => Boolean`

where:
- `graph` is a `@tangle/graph` instance
- `mergeNode` is the proposed merge-node
- `field` *String* is the data field `node.data[field]` you're checking for merge validity

### `overwrite.merge(graph, mergeNode, field) => T`

similar to `isValidMerge`, but return a transformation, `T`
If it cannot, an error is thrown!

---

Used with modules such as:
- @tangle/strategy
- ssb-crut
