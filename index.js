const Validator = require('is-my-json-valid')
const isEqual = require('lodash.isequal')

module.exports = function Overwrite (opts = {}) {
  const {
    identity = {},
    reifiedIdentity = null,
    valueSchema = {
      oneOf: [
        { type: 'string' },
        { type: 'boolean' },
        { type: 'number' },
        { type: 'object' },
        { type: 'null' }
      ]
    }
  } = opts

  const schema = {
    oneOf: [
      {
        type: 'object',
        additionalProperties: false
      },
      {
        type: 'object',
        required: ['set'],
        properties: {
          set: valueSchema
        },
        additionalProperties: false
      }
    ]
  }
  const isValid = Validator(schema)

  function isIdentity (T) {
    return isEqual(T, identity)
  }

  function mapToOutput (T) {
    if (isIdentity(T)) return reifiedIdentity

    return T.set
  }

  function concat (a, b) {
    if (!isValid(a)) throw ConcatError(a)
    if (!isValid(b)) throw ConcatError(b)

    if (isIdentity(b)) return a
    return b
  }

  function mapFromInput (change) {
    return { set: change }
  }

  function isConflict (graph, tipIds, field) {
    if (!Array.isArray(tipIds)) return new Error('tipIds should be an array')
    isConflict.result = null

    // Start with the tips of the graph and traverse backwards through history
    // For each branch record the first non-identity transform found and the nodeId.
    // (this is a "topTip")

    const topTips = {}

    for (const tipId of tipIds) {
      // find first nonIdentity mutation node
      const result = findFirstNonIdentityMutation(tipId, graph, field, isIdentity)

      if (result) topTips[tipId] = result
    }

    // If different transforms are found, check if one is in the history of the other,
    // remove ones that are "lower" topTips
    pruneTopTips(topTips, graph)

    if (Object.values(topTips).length === 0) {
      isConflict.result = identity
      return false
    }
    const result = Object.values(topTips)[0].T
    isConflict.result = result

    if (Object.values(topTips).length === 1) return false

    // Otherwise, check if the tips are different
    return Object.values(topTips).some(function (tip) {
      return !isEqual(result, tip.T)
    })
  }

  function isValidMerge (graph, mergeNode, field) {
    isValidMerge.result = null
    isValidMerge.error = null
    const data = mergeNode.data
    if (field in data && !isIdentity(data[field])) {
      isValidMerge.result = data[field]
      return true
    }
    const isItConflict = isConflict(graph, mergeNode.previous, field)
    if (isItConflict) {
      isValidMerge.error = new Error(`Field ${field} is causing a conflict that is not being merged`)
    } else {
      isValidMerge.result = isConflict.result
    }

    return !isItConflict
  }

  function merge (graph, mergeNode, field) {
    if (isValidMerge(graph, mergeNode, field)) {
      return isValidMerge.result
    } else {
      throw isValidMerge.error
    }
  }

  return {
    schema,
    isValid,
    identity: () => identity,
    concat,
    mapFromInput,
    mapToOutput,
    isConflict,
    isValidMerge,
    merge
  }
}

function findFirstNonIdentityMutation (tipId, graph, field, isIdentity) {
  let result = {
    // nodeId = the id of the node that has mutated the field
    // T = the field transformation
  }

  const queue = [] // new Queue()
  queue.push(tipId)

  while (!('T' in result) && queue.length !== 0) {
    const nextId = queue.shift()
    const data = graph.getNode(nextId).data
    if (field in data && !isIdentity(data[field])) {
      // Once a value is found, record the value and id.
      result = {
        nodeId: nextId,
        T: data[field]
      }
    } else {
      // Otherwise add the previous Ids to the queue.
      for (const prevKey of graph.getPrevious(nextId)) {
        queue.push(prevKey)
      }
    }
  }

  if (!('T' in result || isIdentity(result.T))) return
  // no mutation found in this tips history

  return result
}

function pruneTopTips (topTips, graph) {
  // For each of the topTips, test if it is in the history of other branches
  Object.entries(topTips).forEach(([branchID, branchValue]) => {
    Object.entries(topTips).forEach(([otherID, otherValue]) => {
      if (branchID === otherID) return
      if (graph.getHistory(otherID).includes(branchValue.nodeId)) {
        delete topTips[branchID]
      }
    })
  })
}

function ConcatError (T) {
  return new Error(`cannot concat invalid transformation ${JSON.stringify(T)}`)
}
