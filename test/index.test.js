const test = require('tape')
const { isValid: isValidStrategy } = require('@tangle/strategy')

const Strategy = require('../')
const {
  isValid,
  concat,
  identity,
  mapToOutput,
  mapFromInput
} = Strategy()

test('Strategy.isValid', t => {
  t.true(isValidStrategy(Strategy()), 'is a valid strategy according to @tangle/strategy')
  if (isValidStrategy.error) console.log(isValidStrategy.error)

  t.end()
})

const Ts = [
  { set: 'hello' },
  { set: 'bart was here' },
  { set: '' },
  { set: true },
  { set: null },
  { set: { content: 'dog' } },
  { set: 5 },
  identity()
]

test('schema', t => {
  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    { content: 'dog' },
    undefined,
    { set: undefined }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('schema (valueSchema)', t => {
  const { isValid } = Strategy({
    valueSchema: {
      type: 'number',
      minimum: 0,
      maximum: 13
    }
  })

  const Ts = [
    { set: 0 },
    { set: 7.5 },
    { set: 13 }
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    { set: true },
    { set: '4' },
    { set: -1 },
    { set: 14 }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('mapToOutput', t => {
  Ts.forEach(T => {
    t.deepEqual(
      Ts.map(mapToOutput),
      [
        'hello',
        'bart was here',
        '',
        true,
        null,
        { content: 'dog' },
        5,
        null // default mapToOutput identity
      ],
      'mapToOutput transformation (general + identity)'
    )
  })

  t.end()
})

test('concat, identity + associativity', t => {
  t.equal(concat(identity(), Ts[0]), Ts[0], 'identity (left)')
  t.equal(concat(Ts[0], identity()), Ts[0], 'identity (right)')

  t.equal(
    concat(
      concat(Ts[0], Ts[1]),
      Ts[2]
    ),
    concat(
      Ts[0],
      concat(Ts[1], Ts[2])
    ),
    'associativity'
  )
  t.end()
})

test('mapFromInput', t => {
  t.deepEqual(
    mapFromInput('dog'),
    { set: 'dog' }
  )

  t.end()
})

test('opts.reifiedIdentity', t => {
  const {
    identity,
    mapToOutput
  } = Strategy({
    reifiedIdentity: 'dog'
  })

  t.deepEqual(mapToOutput(identity()), 'dog')
  t.end()
})
