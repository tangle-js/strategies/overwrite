const test = require('tape')
const tangle = require('@tangle/test')

const Strategy = require('../')
const {
  identity,
  isConflict,
  isValidMerge,
  merge
} = Strategy()

const buildGraph = (mermaid) => tangle.buildGraph(mermaid, { dataField: 'title' })

test('merging', t => {
  // merging stuff! ///////////////////////////
  const tips = ['B', 'C']
  t.equal(isConflict(buildGraph(`
    A-->B
    A-->C
  `), tips, 'title'), false, 'no title set')

  t.equal(isConflict(buildGraph(`
    A[{ set: 'cat'}]-->B
    A-->C
  `), tips, 'title'), false, 'no conflict (two identity)')
  t.equal(isConflict(buildGraph(`
    A[{set: 'cat'}]-->B[{set: 'dog'}]
    A-->C
  `), tips, 'title'), false, 'no conflict (one identity)')
  t.equal(isConflict(buildGraph(`
    A[{set: 'cat'}]-->B[{set: 'dog'}]
    A-->C[{set: 'dog'}]
  `), tips, 'title'), false, 'no conflict (same transform)')
  t.equal(isConflict(buildGraph(`
    A[{set: 'cat'}]-->B[{set: 'dog'}]
    A-->C[{set: 'mouse'}]
  `), tips, 'title'), true, 'conflict')

  let mergeNode = buildGraph(`
  A-->B-->D
  A-->C-->D
`).getNode('D')

  t.equal(isValidMerge(buildGraph(`
    A-->B
    A-->C
  `), mergeNode, 'title'), true, 'isValidMerge with only identities')
  t.equal(isValidMerge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C
  `), mergeNode, 'title'), true, 'identity merge is fine for non-conflict')
  t.equal(isValidMerge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C[{set: 'cat'}]
  `), mergeNode, 'title'), false, 'merge that does not resolve conflict is invalid')

  mergeNode.data.title = { set: 'mouse' }
  t.equal(isValidMerge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C[{set: 'cat'}]
  `), mergeNode, 'title'), true, 'nonidentity merge node')

  mergeNode = buildGraph(`
    A-->B-->D
    A-->C-->D
  `).getNode('D')

  t.deepEqual(merge(buildGraph(`
    A-->B
    A-->C
  `), mergeNode, 'title'), identity(), 'merge identities')
  t.deepEqual(merge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C
  `), mergeNode, 'title'), { set: 'dog' }, 'identity merge is fine for non-conflict')
  t.throws(() => merge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C[{set: 'cat'}]
  `), mergeNode, 'title'), /Field title is causing a conflict/, 'merge that does not resolve conflict is invalid')

  mergeNode.data.title = { set: 'mouse' }
  t.deepEqual(merge(buildGraph(`
    A-->B[{set: 'dog'}]
    A-->C[{set: 'cat'}]
  `), mergeNode, 'title'), { set: 'mouse' }, 'nonidentity merge node')

  t.end()
})

test('merging (complex)', t => {
  let mergeNode = buildGraph(`
    A-->B------>M
    A-->C-->D-->M
        C-->E-->M
   `).getNode('M')
  // Two branches, setting in a chain
  t.deepEqual(merge(buildGraph(`
    A-->B
    A-->C-->D
        C-->E
   `), mergeNode, 'title'), identity(), 'merge identities')
  t.deepEqual(merge(buildGraph(`
     A-->B-->M
     A[{set:'dog'}]-->C-->D-->M
                      C-->E-->M
   `), mergeNode, 'title'), { set: 'dog' }, 'only root has title')
  t.deepEqual(merge(buildGraph(`
     A-->B-->M
     A[{set:'dog'}]-->C[{set:'cat'}]-->D-->M
                      C-->E-->M
   `), mergeNode, 'title'), { set: 'cat' }, 'both branches have title')
  t.deepEqual(merge(buildGraph(`
     A-->B-->M
     A[{set:'dog'}]-->C[{set:'cat'}]-->D[{set:'mouse'}]-->M
                      C-->E-->M
   `), mergeNode, 'title'), { set: 'mouse' }, 'Tip overwrites earlier branches')
  t.throws(() => merge(buildGraph(`
    A-->B-->M
    A[{set:'dog'}]-->C[{set:'cat'}]-->D[{set:'mouse'}]-->M
                     C-->E[{set:'kiwi'}]-->M
   `), mergeNode, 'title'), /Field title is causing a conflict/, 'any other node would conflict')

  // Two merge nodes E and M in sequence
  const twoMerge = buildGraph(`
   A-->B[{ set: 'cats' }]->D-->M
   A-->C[{ set: 'dogs' }]->F-->M
       B------------------>E-->M
       C------------------>E
    `)
  mergeNode = twoMerge.getNode('E')
  t.equal(isConflict(twoMerge, ['B', 'C'], 'title'), true, 'Conflict resolving E')
  t.equal(isValidMerge(twoMerge, mergeNode, 'title'), false, 'Conflict resolving E')
  t.throws(() => merge(twoMerge, mergeNode, 'title'), /Field title is causing a conflict/, 'E is invalid merge')
  // t.throws(() => merge(twomerge, 'M', 'title'), /Field title is causing a conflict/,
  // 'invalid merge earlier in graph is not caught')  //Actual result: {set:dogs}

  mergeNode.data.title = { set: 'either' }
  t.deepEqual(merge(buildGraph(`
  A-->B[{ set: 'cats' }]->D-->M
  A-->C[{ set: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ set: 'either' }]
   `), mergeNode, 'title'), { set: 'either' }, 'earlier merge')

  mergeNode = twoMerge.getNode('M')
  t.deepEqual(merge(buildGraph(`
  A-->B[{ set: 'cats' }]->D-->M
  A-->C[{ set: 'dogs' }]->F-->M
      B------------------>E-->M
      C------------------>E[{ set: 'either' }]
   `), mergeNode, 'title'), { set: 'either' }, 'stacked merges')

  const middleX = buildGraph(`
  A-->B-->D[{ set: 'left' }]--->G
      B-->E-------------------->G-->I
  A-->C-->E-------------------->H-->I
      C-->F[{ set: 'right' }]-->H
   `)
  t.deepEqual(merge(middleX, middleX.getNode('G'), 'title'), { set: 'left' }, 'left merge')
  t.deepEqual(merge(middleX, middleX.getNode('H'), 'title'), { set: 'right' }, 'right merge')
  t.throws(() => merge(middleX, middleX.getNode('I'), 'title'), /Field title is causing a conflict/, 'left and right conflict')
  t.end()
})
